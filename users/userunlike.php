<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');

$dbh = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
			   
$user = $_POST['user'];

$qh = $dbh->prepare('DELETE FROM userlikes WHERE liked_user= ? AND liking_user = ?');
$qh->execute([$user, $_SESSION['username']]);

header('Location: profile.php');
?>
