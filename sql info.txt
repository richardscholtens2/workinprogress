CREATE TABLE users (
    user_id INT(11) AUTO_INCREMENT,
    name VARCHAR(100),
    password_hash CHAR(60),
    email VARCHAR(250),
    firstname VARCHAR(100),
    lastname VARCHAR(100),
    placeofbirth VARCHAR(100),
    telephone INT(11),
    bdate DATE,
    pic VARCHAR(100),
    PRIMARY KEY (user_id)
);

CREATE TABLE userlikes (
    liked_user VARCHAR(100),
    liking_user VARCHAR(100)    
);

CREATE TABLE posts (
    id INT(11) AUTO_INCREMENT,
    text VARCHAR(300),
    username VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE postlikes (
    post_id INT(11),
    username VARCHAR(100),
    PRIMARY KEY (post_id)
);