<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');

$dbc = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

$id = $_POST['id'];

$qh = $dbc->prepare('DELETE FROM posts WHERE id = ?');
$qh->execute([$id]);

header('Location: ../index.php');
?>
