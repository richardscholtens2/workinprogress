<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);
?>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Get to work!</title>

  <!-- Bootstrap core CSS -->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <!-- Plugin CSS -->
  <link href="../vendor/magnific-popup/magnific-popup.css" rel="stylesheet" type="text/css">

  <!-- Custom styles for this template -->
  <link href="../css/freelancer.min.css" rel="stylesheet">

</head>


<body id="page-top">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
        <div class="container">
          <a class="navbar-brand js-scroll-trigger" href="#page-top">Work in progress.</a>
          <button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              Menu
              <i class="fa fa-bars"></i>
          </button>
        
          <div class="collapse navbar-collapse" id="navbarResponsive">
              <ul class="navbar-nav ml-auto">

                <li class="nav-item mx-0 mx-lg-1">
                  <?php
                  if (!isset($_SESSION['userId'])) {
                    ?><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="../users/login.php">Log in</a><?
                    }
                  else{
                    ?><a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="../users/logout.php">Log out</a><?
                  }
                    ?>
                </li>
                <li class="nav-item mx-0 mx-lg-1">
                    <form action=../users/search.php method=POST style="padding-top: 10px">
                    <input type=text name=usersearch id=usersearch placeholder='Search User' maxlength=100>
                    <input type=submit value=Search class="btn-primary">
                </form>
                </li>
              </ul>
          </div>
        </div>
    </nav>

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center">


          <?php
            if (!isset($_SESSION['userId'])) {
              ?>
              <div class="container">
              <img class="img-fluid mb-5 d-block mx-auto" src="img/profile.png" alt="">
              <h1 class="text-uppercase mb-0">Get to work!</h1>
              <hr class="star-light">
              <h2 class="font-weight-light mb-0">No money? - Looking for work? - Let's go!</h2>
            </div>
            <?
          }
          else{
            ?><div class="container"><?
            echo "<h1 class=text-center text-uppercase text-secondary mb-0>Welcome " . $_SESSION['username'] . "!</h1>";
                echo "<hr class=star-light mb-5>";
            ?></div><?
          }
        ?>
    </header>

    <section class="login" id="login">
      <div class="container" style="text-align: center">

        <h2 class="text-center text-uppercase text-secondary mb-0">Here you can edit your post</h2>
        <hr class="star-dark mb-5">

        <?php
          require_once('../config.inc.php');

          $db = new PDO("mysql:dbname=$db_name;host=$db_host",
                $db_user, $db_pass,
                [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
  			   
    	    $id = $_POST['id'];

          $query = $db->prepare('SELECT * FROM posts WHERE id = ?');
          $query->execute([$id]);

          foreach ($query as $row) {
            $id = $row['id'];
            $text = htmlspecialchars($row['text']);
        ?>

        <form action=update.php method=POST>
          <input type=hidden name=id value=<?=$id?>>
          <input type=text id=post name=post style="width:500px" maxlength=300 value="<?= htmlspecialchars($text) ?>">
	        <input type=submit value=Update class="btn-secondary">
        </form>
        
        <?php
          }
        ?>
      </div>
    </section>




  <!-- Footer -->
    <footer class="footer text-center">
      <div class="container">
        <div class="row">
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Author:</h4>
            <p class="lead mb-0">J. F. P. (Richard) Scholtens
              <br>s2956586</p>
          </div>
          <div class="col-md-4 mb-5 mb-lg-0">
            <h4 class="text-uppercase mb-4">Around the Web</h4>
            <ul class="list-inline mb-0">
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="http://facebook.nl">
                  <i class="fa fa-fw fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="http://google.nl">
                  <i class="fa fa-fw fa-google-plus"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="http://twitter.com">
                  <i class="fa fa-fw fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="http://linkedin.nl">
                  <i class="fa fa-fw fa-linkedin"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a class="btn btn-outline-light btn-social text-center rounded-circle" href="http://sport050.nl">
                  <i class="fa fa-fw fa-dribbble"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <h4 class="text-uppercase mb-4">College:</h4>
            <p class="lead mb-0">Database-driven Webtechnology<br>
              <a href="https://ddwt.mark.dog">View site.</a></p>
          </div>
        </div>
      </div>
    </footer>

    <div class="copyright py-4 text-center text-white">
      <div class="container">
        <small>Copyright &copy; Work in progress! 2018</small>
      </div>
    </div>

    <!-- Scroll to Top Button (Only visible on small and extra-small screen sizes) -->
    <div class="scroll-to-top d-lg-none position-fixed ">
      <a class="js-scroll-trigger d-block text-center text-white rounded" href="#page-top">
        <i class="fa fa-chevron-up"></i>
      </a>
    </div>

   

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="../vendor/magnific-popup/jquery.magnific-popup.min.js"></script>


    <!-- Custom scripts for this template -->
    <script src="../js/freelancer.min.js"></script>

  </body>

</html>
