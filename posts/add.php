<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');

$dbc = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

$text = $_POST['post'];
$username = $_SESSION['username'];

$qh = $dbc->prepare('INSERT INTO posts (text, username)
                     VALUES (?, ?)');
$qh->execute([$text, $username]);   

header('Location: ../index.php');
?>
