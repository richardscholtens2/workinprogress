<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');

$dbh = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

$text = $_POST['post'];
$id = $_POST['id'];

$qh = $dbh->prepare('UPDATE posts SET text=? WHERE id = ?');

$qh->execute([$text, $id]);   

header('Location: ../index.php');
?>
