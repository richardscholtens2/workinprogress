<?php
session_start();
error_reporting(-1);
ini_set("display_errors", 1);

require_once('../config.inc.php');

$dbh = new PDO("mysql:dbname=$db_name;host=$db_host",
               $db_user, $db_pass,
               [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
			   
$username = $_SESSION['username'];
$id = $_POST['id'];

$qh = $dbh->prepare('DELETE FROM postlikes WHERE username= ? AND post_id = ?');
$qh->execute([$username, $id]);

header('Location: ../index.php');
?>
